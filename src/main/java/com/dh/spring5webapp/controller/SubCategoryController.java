package com.dh.spring5webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dh.spring5webapp.repositories.SubCategoryRepository;

@Controller
public class SubCategoryController {
	
	private SubCategoryRepository subCategoryRepository;
	
	public SubCategoryController(SubCategoryRepository subCategoryRepository) {
		this.subCategoryRepository = subCategoryRepository;
	}

	@RequestMapping("/subcategory")
	public String getSubCategory(Model model) {
		model.addAttribute("subcategories", subCategoryRepository.findAll());
		return "subcategories";
	}
	
}

package com.dh.spring5webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dh.spring5webapp.repositories.ContractRepository;

@Controller
public class ContractController {
	
	private ContractRepository contractRepository;
	
	public ContractController(ContractRepository contractRepository) {
		this.contractRepository = contractRepository;
	}

	@RequestMapping("/contracts")
	public String getCategories(Model model) {
		model.addAttribute("contracts", contractRepository.findAll());
		return "contracts";
	}
	
}

package com.dh.spring5webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dh.spring5webapp.repositories.PositionRepository;

@Controller
public class PositionController {

	private PositionRepository positionRepository;
	
	public PositionController(PositionRepository positionRepository) {
		this.positionRepository = positionRepository;
	}
	
	@RequestMapping("/position")
	public String getPosition(Model model) {
		model.addAttribute("positions", positionRepository.findAll());
		return "positions";
	}	
	
}

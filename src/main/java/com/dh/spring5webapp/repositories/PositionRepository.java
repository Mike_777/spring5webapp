package com.dh.spring5webapp.repositories;

import org.springframework.data.repository.CrudRepository;

import com.dh.spring5webapp.model.Position;

public interface PositionRepository extends CrudRepository<Position, Long>{

}

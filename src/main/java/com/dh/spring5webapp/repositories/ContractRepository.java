package com.dh.spring5webapp.repositories;

import org.springframework.data.repository.CrudRepository;

import com.dh.spring5webapp.model.Contract;

public interface ContractRepository extends CrudRepository<Contract, Long>{

}

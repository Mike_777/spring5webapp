package com.dh.spring5webapp.repositories;

import org.springframework.data.repository.CrudRepository;

import com.dh.spring5webapp.model.SubCategory;

public interface SubCategoryRepository extends CrudRepository<SubCategory, Long>{

}

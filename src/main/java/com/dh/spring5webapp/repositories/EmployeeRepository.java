package com.dh.spring5webapp.repositories;

import org.springframework.data.repository.CrudRepository;

import com.dh.spring5webapp.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Long>{

}

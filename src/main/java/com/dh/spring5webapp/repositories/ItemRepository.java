package com.dh.spring5webapp.repositories;

import org.springframework.data.repository.CrudRepository;

import com.dh.spring5webapp.model.Item;

public interface ItemRepository extends CrudRepository<Item, Long>{

}

package com.dh.spring5webapp.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class) 
public class ModelBase {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	@Column( nullable = false, updatable = false)
	private Date createdOn;

	@LastModifiedDate
	@Temporal(TemporalType.TIMESTAMP)
	//@Column(insertable = false)
	private Date updatedOn;
	
	@Version
	@Column(nullable = false)
	private long version;
	

}